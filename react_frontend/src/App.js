import { BrowserRouter, Route, Switch } from "react-router-dom";
// import { useState, useMemo } from "react";
import Registration from "./components/register/register.js";
import Updatedata from "./components/update/update.js";
import Mainpage from "./components/mainpage/mainpage.js";
import Delete from "./components/delete/delete.js";
import { DeleteSuccess } from "./components/delete/deletesuccess.js";
import { RegisterSuccess } from "./components/register/registersuccess.js";
import { UpdatedSuccess, UpdateSuccess } from "./components/update/updatesuccess.js";
import Getempdata from "./components/get/emp_details.js";
import { Navigation } from "./components/navigation/navigation.js";
import AllEmployeesList from "./components/get/get_all_details.js";
import Emp_details from "./components/get/profilepage2.js";
import { UpdateAll } from "./components/update/updatec_all.js";
import Employees_details from "./components/get/profilepage.js";



function App() {

  return (
    <>
    <BrowserRouter>
    <Navigation/>
      <Switch>
        <Route path="/" exact component={Mainpage} />
        <Route path="/register" exact component={Registration} />
        <Route path="/update/:id" exact component={Updatedata} />
        <Route path="/update_all/:id" exact component={UpdateAll}/>
        <Route path="/delete" exact component={Delete} />
        <Route path="/getempdata" exact component={Getempdata}/>
        <Route path='/all_emp_details' exact component={AllEmployeesList}/>
        <Route path="/registeredSuccessfully" exact component={RegisterSuccess}/>
        <Route path="/deletedsuccessfully" exact component={DeleteSuccess} />
        <Route path="/updatedsuccess" exact component={UpdatedSuccess} />
        <Route path="/updatesuccess" exact component={UpdateSuccess} />
        <Route path="/profiledetails" exact component={Emp_details}/>
        <Route path='/viewempdetails/:id' exact component={Employees_details} />
      </Switch>
    </BrowserRouter>
    </>
  );
}

export default App;
