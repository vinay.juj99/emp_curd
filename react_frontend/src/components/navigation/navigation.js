import {Navbar, Nav} from 'react-bootstrap';
function Navigation(){
    return (
          <Navbar bg="info" variant="dark" expand="sm" fixed="top" className='mb-4'>
              <div>
                  <img src='https://www.pngitem.com/pimgs/m/523-5233379_employee-management-system-logo-hd-png-download.png' alt='' style={{width:"50px", height:"50px" , marginLeft:"8px", borderRadius:"10px"}}/>
              </div>
                                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                                <Navbar.Collapse id="responsive-navbar-nav">
                                    <Nav className="mr-auto">
                                    <Nav.Link href="/">Home</Nav.Link>
                                    <Nav.Link href="/all_emp_details">All Employees</Nav.Link>
                                    <Nav.Link href="/register">Add Employee</Nav.Link>
                                    <Nav.Link href="/getempdata">Get Employee Details</Nav.Link>
                                    <Nav.Link href="/delete">Remove Employee</Nav.Link>
                                    </Nav>
                                </Navbar.Collapse>
                            </Navbar>
      );
}

export {Navigation}