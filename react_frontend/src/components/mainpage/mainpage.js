import './mainpage.css'

function Mainpage(){
        return(
            <div className="maincontainer">
                <img src='https://www.proofhub.com/wp-content/uploads/2021/08/Employee-Management-System-Apps.png' className='image d-none d-md-block' alt=''/>
                <h1 className="d-sm-block d-md-none" style={{marginTop:"30px"}}>Welcome to employee management System</h1>
             </div>
        )
    }

export default Mainpage;