import "./loading.css";

function Loader() {
  return (

    <div className="wrapper">
      <div className="circle"></div>
      <div className="circle"></div>
      <div className="circle"></div>
      <div className="shadow"></div>
      <div className="shadow"></div>
      <div className="shadow"></div>
      <p className="pwrapper">Loading Data...</p>
    </div>
    

  );
}
export default Loader;
