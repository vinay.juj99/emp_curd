import "./loading.css";

function Uloader() {
  return (

    <div className="wrapper">
      <div className="circle"></div>
      <div className="circle"></div>
      <div className="circle"></div>
      <div className="shadow"></div>
      <div className="shadow"></div>
      <div className="shadow"></div>
      <p className="pwrapper">Updating Data...</p>
      </div>
      
  );
}
export default Uloader;
