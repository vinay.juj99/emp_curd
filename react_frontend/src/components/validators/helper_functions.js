
/**
 * This is a helper function to check and validate the email
 * @param {} email this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _email_checker(email) {
  const returnObject = {};
  if (email === undefined) {
    returnObject.email = "email field is mandatory";
    return returnObject;
  }
  if(email.length === 0){
    returnObject.email = "email field is mandatory";
    return returnObject;
  }
  if (typeof email !== "string") {
    returnObject.email = "email must be a string";
    return returnObject;
  }
  if (!email.toLowerCase().match(/[a-z0-9]+@[a-z]+\.[a-z]{2,3}/)) {
    returnObject.email = "email is supposed to be in valid format";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to check and validate the mobile_no
 * @param {} mobile_no this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _mobile_no_checker(phone_number) {
  const returnObject = {};
  if (phone_number === undefined) {
    returnObject.phone_number = "Mobile Number field is mandatory";
    return returnObject;
  }
  if (typeof phone_number !== "number") {
    returnObject.phone_number = "Mobile Number should be a Number type";
    return returnObject;
  }
  if (!Number.isInteger(phone_number)) {
    returnObject.phone_number = "Mobile Number have only Number 0-9" + phone_number;
    return returnObject;
  }
  // if(!phone_number.match(/^[0-9]+$/)){
  //   returnObject.phone_number = "Mobile Number should be a Number type";
  //   return returnObject;    
  // }
  if (phone_number.toString().length !== 10) {
    returnObject.phone_number = "Mobile number should have 10 digits but given number have only " + phone_number.toString().length;
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to check and validate the firstname
 * @param {} firstname this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _firstname_checker(firstname) {
  const returnObject = {};
  // is it a string
  if (firstname === undefined) {
    returnObject.firstname = "Please give Your firstname";
    return returnObject;
  }
  if(!firstname.match(/^[a-zA-Z ]*$/)){
    returnObject.firstname = "Please Enter valid characters"
    return returnObject;
  }
  if (firstname.length < 3) {
    returnObject.firstname =
      "firstname length should be at least 4 characters long";
    return returnObject;
  }
  if (firstname.length > 10) {
    returnObject.firstname = "firstname length shouldn't be 10 characters long";
    return returnObject;
  }
  if (typeof firstname !== "string") {
    returnObject.firstname = "firstname should be of string type";
    return returnObject;
  }
  if (firstname.length === 0) {
    returnObject.firstname = "firstname field is mandatory";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to check and validate the lastname
 * @param {} lastname this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
 function _lastname_checker(lastname) {
  const returnObject = {};
  // is it a string
  if (lastname === undefined) {
    returnObject.lastname = "Please give Your lastname";
    return returnObject;
  }
  if(!lastname.match(/^[a-zA-Z ]*$/)){
    returnObject.lastname = "Please Enter valid characters"
    return returnObject;
  }
  if (typeof lastname !== "string") {
    returnObject.lastname = "lastname should be of string type";
    return returnObject;
  }
  if (lastname.length < 3) {
    returnObject.lastname =
      "lastname length should be at least 4 characters long";
    return returnObject;
  }
  if (lastname.length > 10) {
    returnObject.lastname = "lastname length shouldn't be 10 characters long";
    return returnObject;
  }
  if (lastname.length === 0) {
    returnObject.lastname = "lastname field is mandatory";
    return returnObject;
  }
  return returnObject;
}


/**
 * This is a helper function to check and validate the department
 * @param {} department this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
 function _department_checker(department) {
  const returnObject = {};
  // is it a string
  if (department === undefined) {
    returnObject.department = "Please give Your department";
    return returnObject;
  }
  if (typeof department !== "string") {
    returnObject.department = "department should be of string type";
    return returnObject;
  }
  if (department.length === 0) {
    returnObject.department = "department field is mandatory";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to check and validate the lastname
 * @param {} lastname this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
 function _project_checker(project) {
  const returnObject = {};
  // is it a string
  if (project === undefined) {
    returnObject.project = "Please give Your project";
    return returnObject;
  }
  if (typeof project !== "string") {
    returnObject.project = "project should be of string type";
    return returnObject;
  }
  if (project.length === 0) {
    returnObject.project = "project field is mandatory";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to count the keys in object recieved
 */
function _return_object_keys(obj) {
  
  let returnArray = [];
  for (let key in obj) {
    returnArray.push(key);
  }
  return returnArray;
}

export {

  _firstname_checker,
  _email_checker,
  _mobile_no_checker,
  _return_object_keys,
  _lastname_checker,
  _department_checker,
  _project_checker

};
