import React from "react";
import { _email_checker, _return_object_keys } from "../validators/helper_functions";
import './delete.css';
// import {useContext} from 'react';
// import { UserContext } from '../context/usercontext';
class  Delete extends React.Component {
 
    // const {value, setValue} = useContext(UserContext)
  
    state = {
        email: " ",
        email_err:'',
        email_db_err:'',
        data_err:""
      };
      Success = ()=>{
        const {history} = this.props
        history.push('/deletedsuccessfully')
      }
      apiCallFail = (data) => {
        this.setState({ email_db_err: data.msg });
      };
      changeEmail = (event) => {
        this.setState({ email: event.target.value, email_err:'' });
      };
      // handle on blur to emp_id input in JSX
      validateemail = () => {
        const email = this.state.email;
        const email_errors = _email_checker(email);
        const is_email_validated = _return_object_keys(email_errors).length === 0;
        console.log(email_errors);
        console.log(is_email_validated);
        if (!is_email_validated) {
          this.setState({ email_err: email_errors.email});
        }
      };
     deleteApiCall = async (event) => {
        event.preventDefault();
        console.log(this.state);
 
        const { email  } = this.state
        const url = "http://localhost:4000/delete_details"
        const userDetails = {
            email
        }
        const option = {
          method: "DELETE",
          body: JSON.stringify(userDetails),
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        }; 
        fetch(url, option)
      .then((res) => {
        // Unfortunately, fetch doesn't send (404 error) into the cache itself
        if (res.status >= 400) {
          throw new Error("Server responds with error!");
        }
        return res.json();
      })
      .then(
        (data) => {
          if (data.status === 200) {
            localStorage.setItem("deletedata", JSON.stringify(data))
            this.Success()
          }
          else{
            this.apiCallFail(data)
          }
        },
        (err) => {
          this.setState({
            data_err: "Database not connected, Please Connect to the database",
          });
        }
      );
    }
    render(){
    return (
        <div className="login-page">
            <div className="form">
                <h3>Delete Employee Details</h3>
                <br></br>
                <form className="login-form" onSubmit={this.deleteApiCall}>
                    <input type="text" placeholder="Enter Email" onChange={this.changeEmail}  onBlur={this.validateemail}/>
                    <p style={{ color: "red" }}>{this.state.email_db_err}</p>
                    <p style={{ color: "red" }}>{this.state.email_err}</p>
                    <button>Submit</button>
                    <p style={{ color: "red" }}>{this.state.data_err}</p>
                </form>
            </div>
        </div>
        
        )
    }
}

export default Delete;

