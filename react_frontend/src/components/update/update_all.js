import { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import {
  _email_checker,
  _mobile_no_checker,
  _return_object_keys,
} from "../validators/helper_functions";
import "./update.css";

function Updatealldata() {
  const initialValue = {
    firstname: "",
    lastname: "",
    email: "",
    phone_number: "",
    department: "",
    project: "",
  };
  const [employee, setEmployee] = useState(initialValue);
  const { firstname, lastname, email, phone_number, department, project } =
    employee;
  const { id } = useParams();
//   let history = useHistory();

  useEffect(() => {
    loadempdetails();
  });

  const loadempdetails = () => {
    const url = `http://localhost:4000/emp_details/${id}`;
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    fetch(url, option)
      .then((res) => {
        //  Unfortunately, fetch doesn't send (404 error) into the cache itself
        if (res.status >= 500) {
          throw new Error("Server responds with error!");
        }
        return res.json();
      })
      .then(
        (data) => {
          setEmployee(data);
        },
        (err) => {
          this.setState({
            data_err: "Database not connected, Please Connect to the database",
          });
        }
      );
  };
  const updateallapicall = async(event) => {
    event.preventDefault();
    console.log(employee);
    const { firstname, lastname, phone_number, email, department, project } =
    employee;
      const url = `http://localhost:4000/update_all/${id}`;
      const userDetails = {
        firstname,
        lastname,
        phone_number: parseInt(phone_number),
        email,
        department,
        project,
      };
      const option = {
        method: "PUT",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
      };
      fetch(url, option)
      .then((res) => {
        // Unfortunately, fetch doesn't send (404 error) into the cache itself
        if (res.status >= 500) {
          throw new Error("Server responds with error!");
        }
        return res.json();
      })
      .then(
        (data) => {
          console.log(data)
          if (data.status === 200) {
            // this.signUpSuccess();
            console.log("success")
          }
          else{
            // this.apiCallFail(data)
            console.log("opps")
          }
        },
        // (err) => {
        //   this.setState({
        //     data_err: "Database not connected, Please Connect to the database",
        //   });
        // }
      );
  } 
  return (
    <div className="login-page">
      <div className="form">
        <h3>Update Employee Details</h3>
        <br></br>
        <form className="login-form" onSubmit={()=> updateallapicall}>
            <label>FirstName</label>
          <input
            type="text"
            onChange={(e) =>
              setEmployee({ ...employee, firstname: e.target.value })
            }
            name="firstname"
            
            required
          />
          {/* <p style={{ color: "red" }}>{this.state.email_err}</p> */}
          <label>LastName</label>
          <input
            type="text"
            onChange={(e) =>
              setEmployee({ ...employee, lastname: e.target.value })
            }
            name="lastname"
            value={lastname}
          />
          <label>Email</label>
          <input
            type="text"
            onChange={(e) =>
              setEmployee({ ...employee, email: e.target.value })
            }
            name="email"
            value={email}
          />
          <label>Mobile Number</label>
          <input
            type="text"
            onChange={(e) =>
              setEmployee({ ...employee, phone_number: e.target.value })
            }
            name="phone_number"
            value={phone_number}
          />
          <label>Department</label>
          <input
            type="text"
            onChange={(e) =>
              setEmployee({ ...employee, department: e.target.value })
            }
            name="department"
            value={department}
          />
          <label>Project</label>
          <input
            type="text"
            onChange={(e) =>
              setEmployee({ ...employee, project: e.target.value })
            }
            name="project"
            value={project}
          />
          <button>Update</button>
          {/* <p style={{ color: "red" }}>{this.state.data_err}</p> */}
        </form>
      </div>
    </div>
  );
}
export default Updatealldata;

