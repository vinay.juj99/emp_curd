import React, { Component } from "react";
import {
  _email_checker,
  _mobile_no_checker,
  _return_object_keys,
} from "../validators/helper_functions";
import "./update.css";

class Updatedata extends Component {
  constructor(props) {
    super(props);
    this.state = {
      new_phone_number: "",
      email: "",
      email_err: "",
      email_db_err: "",
      phone_number_err: "",
      data_err:''
    };
  }
  signUpSuccess = () => {
    const { history } = this.props;
    history.push("/updatedsuccess");
  };

  apiCallFail = (data) => {
    this.setState({ email_db_err: data.msg });
  };

  updateApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);
    const { new_phone_number, email } = this.state;
    const url = "http://localhost:4000/update";
    const userDetails = {
      new_phone_number: parseInt(new_phone_number),
      email,
    };
    const option = {
      method: "PUT",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
     };
    // localStorage.setItem("updatedata", JSON.stringify(data));
    fetch(url, option)
    .then((res) => {
      // Unfortunately, fetch doesn't send (404 error) into the cache itself
      if (res.status >= 500) {
        throw new Error("Server responds with error!");
      }
      return res.json();
    })
    .then(
      (data) => {
        if (data.status === 200) {
          localStorage.setItem("updatedata", JSON.stringify(data));
          this.signUpSuccess();
        }
        else{
          this.apiCallFail(data)
        }
      },
      (err) => {
        this.setState({
          data_err: "Database not connected, Please Connect to the database",
        });
      }
    );
  };

  changephone_number = (event) => {
    this.setState({
      new_phone_number: event.target.value,
      phone_number_err: " ",
    });
  };
  // handle on blur to emp_id input in JSX
  validatephone_number = () => {
    const phone_number = this.state.new_phone_number;
    const phone_number_errors = _mobile_no_checker(parseInt(phone_number));
    const is_phone_number_validated =
      _return_object_keys(phone_number_errors).length === 0;
    console.log(phone_number_errors);
    console.log(is_phone_number_validated);
    if (!is_phone_number_validated) {
      // emp_id validation failed
      this.setState({ phone_number_err: phone_number_errors.phone_number });
    }
  };

  changeEmail = (event) => {
    this.setState({ email: event.target.value, email_err: "" });
  };
  // handle on blur to emp_id input in JSX
  validateemail = () => {
    const email = this.state.email;
    const email_errors = _email_checker(email);
    const is_email_validated = _return_object_keys(email_errors).length === 0;
    console.log(email_errors);
    console.log(is_email_validated);
    if (!is_email_validated) {
      // emp_id validation failed
      this.setState({ email_err: email_errors.email });
    }
  };

  render() {
    return (
      <div className="login-page">
        <div className="form">
          <h3>Update Employee Details</h3>
          <br></br>
          <form className="login-form" onSubmit={this.updateApiCall}>
            <input
              type="text"
              placeholder="Enter Email"
              onChange={this.changeEmail}
              onBlur={this.validateemail}
              required
            />
            <p style={{ color: "red" }}>{this.state.email_err}</p>
            <p style={{ color: "red" }}>{this.state.email_db_err}</p>
            <input
              type="text"
              placeholder="Enter Mobile Number"
              onChange={this.changephone_number}
              onBlur={this.validatephone_number}
              required
            />
            <p style={{ color: "red" }}>{this.state.phone_number_err}</p>
            <button>Update</button>
            <p style={{ color: "red" }}>{this.state.data_err}</p>
          </form>
        </div>
      </div>
    );
  }
}
export default Updatedata;
