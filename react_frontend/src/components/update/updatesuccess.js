import './updatesuccess.css'
import React, { useEffect, useState } from "react";
import Uloader from '../loader/uloader';
function UpdatedSuccess(){
  const [done, setDone] = useState(undefined);
  useEffect(()=>{
    setTimeout(()=>{
      setDone(true);
    },2000)
  })
  const data = localStorage.getItem("updatedata")
  const empdata = JSON.parse(data)
    return (
      <>
      {!done ? (
        <Uloader/>
      ) : (
        <div className="animation-ctn">
        <div className="icon icon--order-success svg">
          <svg xmlns="http://www.w3.org/2000/svg" width="154px" height="154px">
            <g fill="none" stroke="#22AE73" strokeWidth={2}>
              <circle cx={77} cy={77} r={72} style={{strokeDasharray: '480px, 480px', strokeDashoffset: '960px'}} />
              <circle id="colored" fill="#22AE73" cx={77} cy={77} r={72} style={{strokeDasharray: '480px, 480px', strokeDashoffset: '960px'}} />
              <polyline className="st0" stroke="#fff" strokeWidth={10} points="43.5,77.8 63.7,97.9 112.2,49.4 " style={{strokeDasharray: '100px, 100px', strokeDashoffset: '200px'}} />
            </g>
          </svg>
        </div>
        <br />
        <h2>Mobile Number Updated Successfully</h2>
        <p>Old Mobile Number : {empdata.old_number}</p>
        <p>New Mobile Number : {empdata.new_phone_number}</p>
      </div>
        )}
      </>
        
    )
}

function UpdateSuccess(){
    return (
      <>
        <div className="animation-ctn">
        <div className="icon icon--order-success svg">
          <svg xmlns="http://www.w3.org/2000/svg" width="154px" height="154px">
            <g fill="none" stroke="#22AE73" strokeWidth={2}>
              <circle cx={77} cy={77} r={72} style={{strokeDasharray: '480px, 480px', strokeDashoffset: '960px'}} />
              <circle id="colored" fill="#22AE73" cx={77} cy={77} r={72} style={{strokeDasharray: '480px, 480px', strokeDashoffset: '960px'}} />
              <polyline className="st0" stroke="#fff" strokeWidth={10} points="43.5,77.8 63.7,97.9 112.2,49.4 " style={{strokeDasharray: '100px, 100px', strokeDashoffset: '200px'}} />
            </g>
          </svg>
        </div>
        <br />
        <h1>Updated Successfully</h1>
      </div>
      </>
        
    )
}

export {UpdatedSuccess , UpdateSuccess}