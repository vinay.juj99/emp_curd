import React, { useEffect, useState } from "react";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import Loader from "../loader/loader";
import { Link , useHistory } from "react-router-dom";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  TableContainer,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles({
  table: {
    width: "100%",
    marginTop: "50px",
  },
  thead: {
    "& > *": {
      fontSize: 18,
      background: "#eb7907",
      color: "#FFFFFF",
    },
  },
  row: {
    "& > *": {
      fontSize: 15,
    },
  },
  button1: {
    backgroundColor: "#0380fc",
  },
  button2: {
    backgroundColor: "#eb0707",
  },
  background: {
    overflowX: "auto",
  },
});

function AllEmployeesList() {
  const [employees, setEmployeees] = useState([]);
  const [done, setDone] = useState(undefined);
  const classes = useStyles();

  useEffect(() => {
    setTimeout(() => {
      const option = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      fetch("http://localhost:4000/get_all_emp_details", option)
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          setEmployeees(data);
          setDone(true);
        });
    }, 2000);
  }, []);

 const SignUpSuccess = () => {
    let  history  = useHistory()
    history.push("/all_emp_details");
  };
 const deleteApicall = (id)=>{
  const url = `http://localhost:4000/deletebyId/${id}`;
  const option = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  };
  fetch(url, option)
        .then((res) => {
          //  Unfortunately, fetch doesn't send (404 error) into the cache itself
          if (res.status >= 500) {
            throw new Error("Server responds with error!");
          }
          return res.json();
        })
        .then(
          (data) => {
            // this.setState({
            //   firstname: data.firstname,
            //   lastname: data.lastname,
            //   phone_number: data.phone_number,
            //   email: data.email,
            //   department: data.department,
            //   project: data.project,
            //   id:data._id
            // });
            console.log(data);
            if(data.status === 200){
              alert("Deleted Successfully")
            }

          },
          (err) => {
            // this.setState({
            //   data_err: "Database not connected, Please Connect to the database",
            // });
            console.log("error")
          }
        );
  }

  return (
    <>
      {!done ? (
        <Loader />
      ) : (
        <div className="d-none d-lg-block">
          <div className={classes.background}>
            <TableContainer className={classes.root}>
              <Table className={classes.table} id="table-to-xls">
                <TableHead>
                  <TableRow className={classes.thead}>
                    <TableCell>FirstName</TableCell>
                    <TableCell>LastName</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Phone Number</TableCell>
                    <TableCell>Department</TableCell>
                    <TableCell>Project</TableCell>
                    <TableCell>Actions</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {employees.map((employee) => (
                    <TableRow className={classes.row} key={employee._id}>
                      <TableCell>{employee.firstname}</TableCell>
                      <TableCell>{employee.lastname}</TableCell>
                      <TableCell>{employee.email}</TableCell>
                      <TableCell>{employee.phone_number}</TableCell>
                      <TableCell>{employee.department}</TableCell>
                      <TableCell>{employee.project}</TableCell>
                      <TableCell>
                      <Link to={`/update_all/${employee._id}`}>
                        <button className="btn btn-info">Edit</button></Link> {' '}
                        <Link to={`/viewempdetails/${employee._id}`}>
                        <button className="btn btn-primary">View</button></Link> {" "}
                        <button className="btn btn-danger" onClick={()=>deleteApicall(employee._id)}>Delete</button>
                      </TableCell>
                      {/* <TableCell>
                  <Button
                    className={classes.button2}
                    variant="contained"
                    component={Link}
                    to={`/delete`}
                  >
                    Delete
                  </Button>{" "}
                </TableCell> */}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              <div>
                <ReactHTMLTableToExcel
                  id="test-table-xls-button"
                  className="btn btn-info"
                  table="table-to-xls"
                  filename="AllEmployeeDetails"
                  sheet="tablexls"
                  buttonText="Export"
                />
              </div>
            </TableContainer>
          </div>
        </div>
      )}
      {/* // <div class="row row-cols-xs-2 row-cols-md-2 d-md-block d-lg-none mt-5"></div> */}
      <div className="row row-cols-xs-6 row-cols-md-1 d-md-block d-lg-none mt-5">
        {employees.map((employee) => (
          <div className="col m-4" key={employee.id}>
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">
                  {employee.firstname + " " + employee.lastname}
                </h5>
                <ul className="list-group">
                  <li className="list-group-item list-group-item-primary">
                    First Name : {employee.firstname}
                  </li>
                  <li className="list-group-item list-group-item-secondary">
                    Last Name : {employee.lastname}
                  </li>
                  <li className="list-group-item list-group-item-success">
                    Email : {employee.email}
                  </li>
                  <li className="list-group-item list-group-item-info">
                    Mobile Number : {employee.phone_number}
                  </li>
                  <li className="list-group-item list-group-item-light">
                    Department : {employee.department}
                  </li>
                  <li className="list-group-item list-group-item-dark">
                    Project : {employee.project}
                  </li>
                </ul>
                <Link to={`/update_all/${employee._id}`}>
                <button className="btn btn-primary m-3">Edit</button></Link>
                <Link to={`/viewempdetails/${employee._id}`}>
                <button className="btn btn-info">View</button></Link>
                <button className="btn btn-danger m-3" onClick={()=>deleteApicall(employee._id)}>Delete</button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

export default AllEmployeesList;
