import React, { Component } from "react";
import { _email_checker, _return_object_keys } from "../validators/helper_functions";
import "./employee.css";

class Getempdata extends Component {
    state= {
      email : " ",
      email_db_err:'',
      email_err:''
    }

  Success = () => {
    const { history } = this.props;
    history.push("/profiledetails");
  };

  apiCallFail = (data) => {
    this.setState({ email_db_err: data.msg });
  };

   getApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state)
    const {email} = this.state
    const url = `http://localhost:4000/get_emp_details`;
    const userDetails = {
      email
  }
    const option = {
      method: "POST",
      body : JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    this.setState({ Empdata: data });
    if (data.status_code === 200){
      console.log(data)
      // const EmployeeContext = React.createContext(data);
      // console.log(EmployeeContext)
      localStorage.setItem("empdata",JSON.stringify(data))
      this.Success()
    } else{
      this.apiCallFail(data)
    }
  };
  changeEmail = (event) => {
    this.setState({ email: event.target.value, email_err:'' });
  };
  // handle on blur to emp_id input in JSX
  validateemail = () => {
    const email = this.state.email;
    const email_errors = _email_checker(email);
    const is_email_validated = _return_object_keys(email_errors).length === 0;
    console.log(email_errors);
    console.log(is_email_validated);
    if (!is_email_validated) {
      // emp_id validation failed
      this.setState({ email_err: email_errors.email});
    }
  };
  render() {
    return (
      <div className="main">
        <div className="login-page">
            <div className="form">
                <h3>Get Employee Details</h3>
                <br></br>
                <form className="login-form" onSubmit={this.getApiCall}>
                    <input type="text" placeholder="Enter Email" onChange={this.changeEmail} onBlur={this.validateemail}/>
                    <p style={{ color: "red" }}>{this.state.email_db_err}</p>
                    <p style={{ color: "red" }}>{this.state.email_err}</p>
                    <button>Submit</button>
                </form>
            </div>
        </div>
      </div>
    );
  }
}
export default Getempdata;
