import { Card, Button, ListGroup, ListGroupItem } from "react-bootstrap";
import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import Loader from "../loader/loader";

const Emp_details = () => {
  const [done, setDone] = useState(undefined);
  useEffect(()=>{
    setTimeout(()=>{
      setDone(true);
    },2000)
  })
    const data = localStorage.getItem("empdata")
    const empdata = JSON.parse(data)
  return (
    <>
    {!done ? (
        <Loader/>
      ) : (
        <Card style={{ width: "18rem" , marginLeft:"auto", marginRight:"auto", marginTop:"80px" }}>
        <Card.Img variant="top" src="https://www.seekpng.com/png/detail/966-9665317_placeholder-image-person-jpg.png" className="img-fluid" style={{ height:"220px", borderRadius:"20px" }}/>
        <Card.Body>
          <Card.Title>{empdata.obj.firstname +' '+ empdata.obj.lastname}</Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>First Name : {empdata.obj.firstname}</ListGroupItem>
          <ListGroupItem>Last name : {empdata.obj.lastname}</ListGroupItem>
          <ListGroupItem>Email : {empdata.obj.email}</ListGroupItem>
          <ListGroupItem>Mobile Number : {empdata.obj.phone_number}</ListGroupItem>
          <ListGroupItem>Department : {empdata.obj.department}</ListGroupItem>
          <ListGroupItem>Project : {empdata.obj.project}</ListGroupItem>
        </ListGroup>
        <Card.Body>
            <Link to='/update'>
          <Button>Edit</Button></Link>
        </Card.Body>
      </Card>
        )}
    </>

  );
};

export default Emp_details;
