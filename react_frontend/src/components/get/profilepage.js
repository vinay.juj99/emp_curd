import {Link} from 'react-router-dom';
import React, { Component } from "react";
class Employees_details extends Component{
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      phone_number: "",
      email: "",
      department: "",
      project: "",
      id:""
    };
  }
  componentDidMount() {
    let id = this.props.match.params.id;
    console.log(id);
    // console.log(id)
    this.loadempdetails(id);
  }
    loadempdetails = (id) => {
      const url = `http://localhost:4000/emp_details/${id}`;
      const option = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      fetch(url, option)
        .then((res) => {
          //  Unfortunately, fetch doesn't send (404 error) into the cache itself
          if (res.status >= 500) {
            throw new Error("Server responds with error!");
          }
          return res.json();
        })
        .then(
          (data) => {
            this.setState({
              firstname: data.firstname,
              lastname: data.lastname,
              phone_number: data.phone_number,
              email: data.email,
              department: data.department,
              project: data.project,
              id:data._id
            });
            console.log(data);
          },
          (err) => {
            this.setState({
              data_err: "Database not connected, Please Connect to the database",
            });
          }
        );
    };
    render(){
  return (
    <div class="container">
    <div class="col-md-6 col-12">
        <div class="col m-4">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">
                {this.state.firstname + " " + this.state.lastname}
              </h5>
              <ul class="list-group">
                <li class="list-group-item list-group-item-primary">
                  First Name : {this.state.firstname}
                </li>
                <li class="list-group-item list-group-item-secondary">
                  Last Name : {this.state.lastname}
                </li>
                <li class="list-group-item list-group-item-success">
                  Email : {this.state.email}
                </li>
                <li class="list-group-item list-group-item-info">
                  Mobile Number : {this.state.phone_number}
                </li>
                <li class="list-group-item list-group-item-light">
                  Department : {this.state.department}
                </li>
                <li class="list-group-item list-group-item-dark">
                  Project : {this.state.project}
                </li>
              </ul>
              <Link to={`/update_all/${this.state.id}`}>
                <button className="btn btn-primary m-3">Edit</button></Link>
              <button type="submit" class="btn btn-danger m-3">Delete</button>  
            </div>
          </div>
        </div>
      </div>
</div>
  )
  }
};
export default  Employees_details;